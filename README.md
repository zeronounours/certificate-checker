# Certificate checker

Script to check validity of SSL certificates and warm about expired one, or
the one to be soon expired.

## Getting started

To install the checker, just create a cron task, based on the template in the
repository.
```
cp certificate_checker.cron /etc/cron.d/certificate_checker
```

Then update created cron file with the full path to script.

At every execution, the script search for certificates in the SSL root
directory (configurable). It reports expired certificates and certificates to
be soon expired by mail. It uses `sendmail` for that purpose and send reports
to the root user of the server.

## Configuration

  Usage: check_certificates.sh <days> [<ssl_root_directory> [<cert_ext>]]

The script takes from 1 to 3 arguments:
1. `days`: the number of days prior to expiration to start warning about it
2. `ssl_root_directory`: the directory where to recursively search for
certificates. Default: `/etc/ssl`
3. `cert_ext`: extension of a certificate file. Default: `crt`

Some additional variables may be configured at the begining of the script:
* `MAIL_FROM`: sender of the mail
* `MAIL_TO`: recipient of the mail
* `MAIL_HEADER`: header of the mail

## License
The repository is under MIT license.
