#!/bin/bash

# A script which verifies all certificate validity period and send a mail
#
# Usage: ./check_certificates <days before warning> [<root directory> [<certiticate extension>]]
# If no root directory is provided, use /etc/ssl
# if no certificate extension is provided, use crt

HOSTNAME="$(hostname)"

MAIL_FROM="Certificate checker <certificate@$HOSTNAME>"
MAIL_TO="root@$HOSTNAME"
MAIL_HEADER="Message-id: <$(date +%Y%m%d.%H%M%S)-\
$(hexdump -vn 10 -e '1/1 "%02x"' /dev/urandom)@$HOSTNAME>
From: $MAIL_FROM
To: $MAIL_TO
Subject: Expiration date of some certificates is close
Date: $(LANG=US date +"%a, %e %b %Y %H:%M:%S %z")
Content-type: text/plain; charset=UTF-8

"

DAYS="$(( $1 * 24 * 3600 ))"
ROOT="${2:-/etc/ssl}/"
EXT="${3:-crt}"

declare -a warning expired
now=$(date +%s)

for cert in $(find $ROOT -name "*.$EXT"); do
  expiration="$(date -d "$(
    openssl x509 -in "$cert" -noout -enddate | cut -d= -f2-
    )" +%s)"

  if [[ $expiration < $now ]]; then
    expired=("${expired[@]}"
    "${cert#$ROOT} has expired $(( ($now - $expiration) / 3600 / 24 )) \
days ago ($(date -d @$expiration))"
    )
  elif [[ $expiration < $(($now + $DAYS)) ]]; then
    warning=("${warning[@]}"
    "${cert#$ROOT} will expire in $(( ($expiration - $now) / 3600 / 24 )) \
days ($(date -d @$expiration))"
    )
  fi
done

msg=""

if [[ ${#expired[@]} != 0 ]]; then
  msg="$msg
EXPIRED CERTIFICATES
====================
"
  for line in "${expired[@]}"; do
    msg="$msg
* $line"
  done
  msg="$msg

"
fi

if [[ ${#warning[@]} != 0 ]]; then
  msg="$msg
To expire certificates
======================
"
  for line in "${warning[@]}"; do
    msg="$msg
* $line"
  done
  msg="$msg

"
fi

if [[ "$msg" != "" ]]; then
  cat <<<"$MAIL_HEADER $msg" | sendmail -t
fi
